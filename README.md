## How to use this driver ##

* Clone/download the codes

```
#!shell

make clean
make 
sudo make install 

```

Connect MT7610U Wifi Dongle to your PC / Laptop.. 

For other chipsets some files need modification including the following

* Makefile
* os/linux/config.mk 
* common/rtusb_dev_id.c

```
If you use TLP, please make sure TLP not to auto-suspend your dongle.
```